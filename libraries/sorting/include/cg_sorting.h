/**
 * @file cg_sorting.h
 *
 * a file for handling different sorting algorithms
 */


#pragma once


// defines


// std includes
#include <vector>


// 3rd party includes


// my includes


// namespace
namespace cg
{
	// enums


	// structs


	// classes


	// types


	// constants


	// variables


	// functions
	template <class type>
	void Bubble_Sort(
		std::vector< type > array )
	{
		unsigned int i,j;
		unsigned int s = array.size();
		type tmp;

		for( i = 0; i < s - 1; i++ )
		{
			for( j = i + 1; j < s; j++ )
			{
				if( array[ i ] > array[ j ] )
				{
					tmp			= array[ i ];
					array[ i ]	= array[ j ];
					array[ j ]	= tmp;
				}
			}
		}
	}
}


/* eof */