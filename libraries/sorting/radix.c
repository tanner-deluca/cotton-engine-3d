#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define B10 10


void print(
	char* title,
	int arr[],
	int size )
{
	printf( "%s", title );

	for( int i = 0; i < size; i++ )
		printf( " %i", arr[ i ] );
	printf( "\n" );
}


int max(
	int arr[],
	int size )
{
	int m = arr[ 0 ];
	for( int i = 1; i < size; i++ )
	{
		if( arr[ i ] > m )
			m = arr[ i ];
	}


	return m;
}


void base10(
	int arr[],
	int s,
	int m )
{
	int* cpy = ( int* )malloc( sizeof( int ) * s );
	memcpy( cpy, arr, sizeof( int ) * s );

    for( int exp = 1; m / exp > 0; exp *= 10 )
    {
        // countSort(cpy, s, exp);
    	int tmp;
    	for( int a = 0; a < s - 1; a++ )
    	{
    		for( int b = a; b < s; b++ )
    		{
    			if( ( cpy[ a ] / exp ) % 10 > ( cpy[ b ] / exp ) % 10 )
    			{
    				tmp = cpy[ a ];
    				cpy[ a ] = cpy[ b ];
    				cpy[ b ] = tmp;
    			}
    		}
    	}
    	// print( "base10", cpy, s );
    }

    print( "base10", cpy, s );
    free( cpy );
}


void binary(
	int arr[],
	int s,
	int m )
{
	int* cpy = ( int* )malloc( sizeof( int ) * s );
	memcpy( cpy, arr, sizeof( int ) * s );

	for( int shift = 0; shift < sizeof( int ); shift++ )
	{
		int mask = 0xff << ( shift * 8 );
		int tmp;
    	for( int a = 0; a < s - 1; a++ )
    	{
    		for( int b = a; b < s; b++ )
    		{
    			if( ( cpy[ a ] & mask ) > ( cpy[ b ] & mask ) )
    			{
    				tmp = cpy[ a ];
    				cpy[ a ] = cpy[ b ];
    				cpy[ b ] = tmp;
    			}
    		}
    	}

		// print( "binary", cpy, s );
	}


    print( "binary", cpy, s );
    free( cpy );
}


int main(
	int argc,
	char** argv )
{
	int arr[] = { 170, 45, 75, 90, 802, 24, 2, 66 };
	int s = sizeof( arr ) / sizeof( int );
	int m = max( arr, s );

	print( "unsorted", arr, s );
	printf( "max: %i\n", m );
	base10( arr, s, m );
	binary( arr, s, m );

	return 0;
}


// /* radix sort */
// #include <stdio.h>
// #include <stdlib.h>
// #include <string.h>



// void print(
// 	char* title,
// 	int arr[],
// 	int size )
// {
// 	printf( "%s", title );

// 	for( int i = 0; i < size; i++ )
// 		printf( " %i", arr[ i ] );
// 	printf( "\n" );
// }


// int max(
// 	int arr[],
// 	int size )
// {
// 	int m = arr[ 0 ];
// 	for( int i = 1; i < size; i++ )
// 	{
// 		if( arr[ i ] > m )
// 			m = arr[ i ];
// 	}


// 	return m;
// }


// void base10(
// 	int arr[],
// 	int size )
// {
// 	int* cpy = ( int* )malloc( sizeof( int ) * size );
// 	memcpy( cpy, arr, sizeof( int ) * size );

// 	int m = max( cpy, size );

// 	for( int exp = 1; m / exp > 1; exp *= 10 )
// 	{
// 		int i = 0;
// 		int* out = ( int* )malloc( sizeof( int ) * size );
// 		int count[ 10 ] = { 0 };

// 		for( i = 1; i < size; i++ )
// 			count[ ( cpy[ i ] / exp ) % 10 ]++;

// 		for( i = 1; i < 10; i++ )
// 			count[ i ] += count[ i - 1 ];

// 		for( i = size - 1; i >= 0; i-- )
// 		{
// 			out[ count[ (cpy[ i ] / exp ) % 10 ] - 1 ] = cpy[ i ];
//         	count[ ( cpy[ i ] / exp ) % 10 ]--;
// 		}

// 		for( i = 1; i < size; i++ )
// 			cpy[ i ] = out[ i ];
// 		// free( out );
// 		print( "base10", cpy, size );
// 	}


// 	print( "base10", cpy, size );
// 	free( cpy );
// }


// void binary(
// 	int arr[],
// 	int size )
// {
// 	int* cpy = ( int* )malloc( sizeof( int ) * size );
// 	memcpy( cpy, arr, sizeof( int ) * size );

// 	print( "binary", cpy, size );
// 	free( cpy );
// }


// int main(
// 	int argc,
// 	char** argv )
// {
// 	int arr[] = { 170, 45, 75, 90, 802, 24, 2, 66 };
// 	int size = sizeof( arr ) / sizeof( int );
// 	print( "unsorted", arr, size );
// 	base10( arr, size );

// 	return 0;
// }


// /* eof */