/**
 * @file cmd-line.h
 *
 * a file for handling command line options
 */


#pragma once


// defines


// std includes
#include <string>
#include <vector>


// 3rd party includes


// my includes


namespace CG
{
	namespace Command_Line
	{
		// enums
		enum Option_Types
		{
			NONE,
			STRING,
			INTEGER,
			DECIMAL,
		};


		// types
		typedef void ( *Option_Callback )( void* arg );


		// structs
		struct Option
		{
			char			alias		= ' ';
			std::string		name		= "";

			bool			exit_after	= false;

			Option_Types	type		= Option_Types::NONE;
			Option_Callback	callback	= nullptr;
		};


		// classes


		// types


		// constants


		// variables


		// functions
		std::vector< std::string > Parse_Options(
			int argc,
			char* argv[],
			std::string usage,
			std::vector< Option > options );
	}
}

/* eof */