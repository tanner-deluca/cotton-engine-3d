/**
 * @file test.cpp
 *
 * a file for testing command line options
 */


// defines


// std includes
#include <iostream>


// 3rd party includes


// my includes
#include "cmd-line.h"


// namespaces


// enums


// structs


// classes


// constants


// variables


// function declarations


// functions
int main(
    int argc,
    char* argv[] )
{
    std::cout << "hi\n";
    
    return 0;
}


/* eof */