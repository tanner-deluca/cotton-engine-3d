/**
 * @file cmd-line.cpp
 *
 * a file for handling command line options
 */


// defines


// std includes
#include "iostream"


// 3rd party includes


// my includes
#include "cmd-line.h"


// namespaces


// enums


// structs


// classes


// constants


// variables


// function declarations


// functions
std::vector< std::string > CG::Command_Line::Parse_Options(
	int argc,
	char* argv[],
	std::string usage,
	std::vector< CG::Command_Line::Option > options )
{
	// setup
	std::vector< std::string >	retval;
	std::string					input;


	// parse flags
	for( int i = 0; i < argc; i++ )
	{
		input = std::string( argv[ i ] );

		if( input.at( 0 ) == '-' )
		{
			if( input.length() == 1 )
			{
				goto error;
			}
			else if( input.at( 1 ) == '-' )
			{

			}
			else
			{
				
			}
		}
		else
		{
			retval.push_back( input );
		}
	}





	return retval;


	error:
		std::cout << usage << "\n";
		exit( 1 );
}


/* eof */